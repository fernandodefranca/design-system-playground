import React from 'react'

export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props)
    this.state = { hasError: false, error: null }
  }

  static getDerivedStateFromError(error) {
    return { hasError: true, error }
  }

  componentDidCatch(error, info) {
    // eslint-disable-next-line
    console.error(error, info)
  }

  render() {
    const { hasError, error } = this.state
    const { children } = this.props

    if (hasError) {
      return (
        <div>
          <h2>Oops</h2>
          <p>This component caused an error.</p>
          <pre>{error.message}</pre>
        </div>
      )
    }

    return children
  }
}
