import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import theme from '../theme'

function clampNumber(number, min, max) {
  return Math.max(min, Math.min(number, max))
}

function buildArrayWithLength(len) {
  const arr = []
  for (let i = 0; i < len; i++) {
    arr.push(true)
  }
  return arr
}

const Wrapper = styled.div`
  display: flex;
`

const Star = styled.div`
  font-size: ${({ size }) => `${size}px`};
  width: ${({ size }) => `${size}px`};
  height: ${({ size }) => `${size}px`};
  padding: ${({ size }) => `${size / 8}px`};
  color: ${({ active }) => (active ? theme.backgroundColor.primary : '#ccc')};
  cursor: pointer;
`
/**
 * A 5 star rating component
 */
function Rating({ value, size, onChange }) {
  const clampedValue = clampNumber(parseInt(value, 10), 0, 5)
  const clampedSize = clampNumber(size, 16, 48)
  const arr = buildArrayWithLength(5)
  let inc = 0

  return (
    <Wrapper className="Rating">
      <Star
        size={clampedSize}
        onClick={() => {
          if (onChange) onChange(0)
        }}>
        ✖️
      </Star>
      {arr.map((val, index) => {
        const isActive = index < clampedValue
        inc++
        if (isActive)
          return (
            <Star
              active
              key={`active-${inc}`}
              size={clampedSize}
              onClick={() => {
                if (onChange) onChange(index + 1)
              }}>
              🧡
            </Star>
          )
        return (
          <Star
            key={`inactive-${inc}`}
            size={clampedSize}
            onClick={() => {
              if (onChange) onChange(index + 1)
            }}>
            🖤
          </Star>
        )
      })}
    </Wrapper>
  )
}

Rating.propTypes = {
  /**
   * Size (in pixels)
   */
  size: PropTypes.number,
  /**
   * From 0 to 5
   */
  value: PropTypes.number,
  /**
   * Rating change handler function
   */
  onChange: PropTypes.func,
}

Rating.defaultProps = {
  size: 24,
  value: 0,
}

export default React.memo(Rating)
