import React from 'react'
import Component from './'
import renderer from 'react-test-renderer'

it('Renders correctly', () => {
  const tree = renderer.create(<Component />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "value" prop of 1', () => {
  const tree = renderer.create(<Component value={1} />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "value" prop of 3', () => {
  const tree = renderer.create(<Component value={3} />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "value" prop of 5', () => {
  const tree = renderer.create(<Component value={5} />).toJSON()
  expect(tree).toMatchSnapshot()
})
