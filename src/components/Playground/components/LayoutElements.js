import styled from 'styled-components'
import styles from '../../styles'

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  height: 100vh;
`

const LeftPanel = styled.div`
  ${styles.type}
  min-width: 320px;
  flex: 0 1 auto;
  flex-direction: column;
  padding: 16px;
  size: 16px;
  overflow-y: scroll;
`
const MainPanel = styled.div`
  display: flex;
  flex: 1 1 0%;
  flex-direction: column;
`

const DemoPanel = styled.div`
  flex: 1 1 0%;
  padding: 16px;
  background-color: #eee;
  overflow-y: auto;
`

const CodePanel = styled.div`
  flex: 0 1 auto;
  background: rgb(40, 42, 54);
  padding: 0 16px;
`

export { Wrapper, LeftPanel, MainPanel, DemoPanel, CodePanel }
