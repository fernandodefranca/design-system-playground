import React from 'react'
import PropTypes from 'prop-types'
import styled, { keyframes } from 'styled-components'

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const SpinnerWithStyle = styled.div`
  animation: ${rotate360} 0.5s linear infinite;
  width: ${({ size }) => size}px;
  height: ${({ size }) => size}px;
  margin: ${({ size }) => size / 2}px;
  border-radius: 50%;
  border-top: 2px solid #aaa7;
  border-right: 2px solid #aaa7;
  border-bottom: 2px solid #aaa7;
  border-left: 2px solid #aaaf;
  display: inline-block;
`

function Spinner({ children, ...props }) {
  return <SpinnerWithStyle {...props} />
}

Spinner.propTypes = {
  size: PropTypes.number,
}

Spinner.defaultProps = {
  size: 12,
}

export default Spinner
