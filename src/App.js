import React from 'react'
import Playground from 'components/Playground'

import Button from 'design-system-components/Button'
import Spinner from 'design-system-components/Spinner'
import Rating from 'design-system-components/Rating'

import componentsInfo from './parsed-components-info.json'

const componentsDefs = { Button, Spinner, Rating }

function App() {
  const props = { componentsInfo, componentsDefs }
  return (
    <div className="App">
      <Playground {...props} />
    </div>
  )
}

export default App
