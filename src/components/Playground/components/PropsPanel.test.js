import React from 'react'
import ReactDOM from 'react-dom'
import Component from './PropsPanel'

it('Given proper configuration PropsPanel component renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Component {...props} />, div)
  ReactDOM.unmountComponentAtNode(div)
})

const props = {
  componentInfo: {
    description: 'A Button component',
    displayName: 'Button',
    methods: [],
    props: {
      value: {
        type: {
          name: 'string',
        },
        required: false,
        description: 'The button text',
        defaultValue: {
          value: "'button label'",
          computed: false,
        },
      },
      mode: {
        type: {
          name: 'enum',
          value: [
            {
              value: "'primary'",
              computed: false,
            },
            {
              value: "'secondary'",
              computed: false,
            },
          ],
        },
        required: false,
        description: 'Determines the button role and style',
        defaultValue: {
          value: "'primary'",
          computed: false,
        },
      },
      size: {
        type: {
          name: 'enum',
          value: [
            {
              value: "'regular'",
              computed: false,
            },
            {
              value: "'big'",
              computed: false,
            },
          ],
        },
        required: false,
        description: 'Determines the button size',
        defaultValue: {
          value: "'regular'",
          computed: false,
        },
      },
      disabled: {
        type: {
          name: 'bool',
        },
        required: false,
        description: 'Disables the button',
        defaultValue: {
          value: 'false',
          computed: false,
        },
      },
      loading: {
        type: {
          name: 'bool',
        },
        required: false,
        description: 'Display a spinner to indicate an async operation',
        defaultValue: {
          value: 'false',
          computed: false,
        },
      },
      fluid: {
        type: {
          name: 'bool',
        },
        required: false,
        description: "Determines it's layout behaviour",
        defaultValue: {
          value: 'false',
          computed: false,
        },
      },
      onClick: {
        type: {
          name: 'func',
        },
        required: false,
        description: 'Click event handler function',
        defaultValue: {
          value: "() => {\n  console.log('Button was clicked.')\n}",
          computed: false,
        },
      },
    },
  },
  currentProps: {
    value: 'button label',
    mode: 'primary',
    size: 'regular',
    disabled: false,
    loading: false,
    fluid: false,
  },
  onChange: () => {},
}
