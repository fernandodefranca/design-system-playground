import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import theme from '../theme'
import Spinner from '../Spinner'

const { colors, sizes, padding, backgroundColor } = theme

const ButtonWithStyle = styled.button`
  font-family: -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif;
  color: ${({ mode }) => colors[mode] || colors.primary};
  border: none;
  border-radius: 4px;
  size: ${({ size }) => sizes[size] || '16px'};
  padding: ${({ size }) => padding[size] || '16px'};
  cursor: ${({ disabled }) => (disabled ? 'unset' : 'pointer')};
  opacity: ${({ disabled }) => (disabled ? 0.4 : 1)};
  display: ${({ fluid }) => (fluid ? 'block' : 'inline-block')};
  width: ${({ fluid }) => (fluid ? '100%' : 'auto')};
  background-color: ${({ mode }) => backgroundColor[mode] || backgroundColor.primary};
`

const InnerWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

/**
 * A Button component
 */
function Button(props) {
  const { value, loading, disabled, ...otherProps } = props
  return (
    <ButtonWithStyle type="button" disabled={disabled} {...otherProps}>
      <InnerWrapper>
        {value}
        {loading && <Spinner size={8} />}
      </InnerWrapper>
    </ButtonWithStyle>
  )
}

Button.propTypes = {
  /**
   * The button text
   */
  value: PropTypes.string.isRequired,
  /**
   * Determines the button role and style
   */
  mode: PropTypes.oneOf(['primary', 'secondary']),
  /**
   * Determines the button size
   */
  size: PropTypes.oneOf(['regular', 'big']),
  /**
   * Disables the button
   */
  disabled: PropTypes.bool,
  /**
   * Display a spinner to indicate an async operation
   */
  loading: PropTypes.bool,
  /**
   * Determines it's layout behaviour
   */
  fluid: PropTypes.bool,
  /**
   * Click event handler function
   */
  onClick: PropTypes.func,
}

Button.defaultProps = {
  value: 'button label',
  mode: 'primary',
  size: 'regular',
  disabled: false,
  loading: false,
  fluid: false,
  onClick: () => {
    console.log('Button was clicked.')
  },
}

export default React.memo(Button)
