# Design system playground

## Common tasks (testing, running)

- `build-and-serve` will generate a production build and start a server on port 5000 (if available).
- `yarn test` will run all tests.

## Observations

- Development environment: node v10.16.0
- Some components are stateless.
- Therefore the rating component will not update itself when clicked.
- On the other hand the component will update following the settings made on the props panel (at the left).
