import React from 'react'
import ReactDOM from 'react-dom'
import Component from './Selector'

it('Given proper configuration Selector component renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Component {...props} />, div)
  ReactDOM.unmountComponentAtNode(div)
})

it('Given alternative configuration Selector component renders without crashing', () => {
  const div = document.createElement('div')
  const alternateProps = { ...props, value: '' }
  ReactDOM.render(<Component {...alternateProps} />, div)
  ReactDOM.unmountComponentAtNode(div)
})

const props = {
  value: 'Button',
  options: ['Button', 'Spinner', 'Rating'],
  onChange: () => {},
}
