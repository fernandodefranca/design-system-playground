export default function getComponentDefaultProps(componentProps) {
  if (!componentProps) return {}

  return Object.keys(componentProps).reduce((acc, propName) => {
    const { defaultValue, type } = componentProps[propName]
    if (defaultValue) {
      let { value } = defaultValue

      if (type.name === 'func') return acc
      if (type.name === 'number') value = parseFloat(value)
      if (type.name === 'bool') value = value === 'true'

      // Remove extra quotes from strings
      if (typeof value === 'string') value = value.split("'").join('')
      acc[propName] = value
    }
    return acc
  }, {})
}
