import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import PropField from './PropField'

const Header = styled.div`
  margin-top: 16px;
  margin-bottom: 16px;
`

const Title = styled.h2`
  margin: 0;
`

const Description = styled.p`
  margin: 0;
  color: #808080;
`

const Row = styled.div`
  padding-top: 24px;
  border-top: 1px solid #eee;
  margin-top: 24px;
`

const PropHeader = styled.div`
  font-weight: normal;
`

const FieldWrapper = styled.div`
  margin-top: 8px;
`

const FieldDescription = styled.div`
  font-size: 12px;
  color: #808080;
`

export default function PropsPanel({ componentInfo, currentProps, onChange }) {
  if (!componentInfo) return null

  const { displayName, description, props } = componentInfo

  return (
    <React.Fragment>
      <Header>
        <Title>{displayName}</Title>
        {description && <Description>{description}</Description>}
      </Header>
      {Object.keys(props).map((propName) => {
        const propInfo = props[propName]
        const { type, required, description: propDescription } = propInfo
        return (
          <Row key={propName}>
            <PropHeader>
              <label htmlFor={propName}>
                {propName} [{type.name}] {required ? '(*)' : null}
              </label>
              <FieldDescription>{propDescription}</FieldDescription>
            </PropHeader>
            <FieldWrapper>
              <PropField
                fieldType={type}
                fieldName={propName}
                fieldValue={currentProps[propName]}
                onChange={onChange}
              />
            </FieldWrapper>
          </Row>
        )
      })}
    </React.Fragment>
  )
}

PropsPanel.propTypes = {
  componentInfo: PropTypes.object,
  currentProps: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
}

PropsPanel.defaultProps = {
  componentInfo: null,
}
