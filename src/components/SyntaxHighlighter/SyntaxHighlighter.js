import React from 'react'
import SyntaxHighlighter from 'react-syntax-highlighter'
import style from './style'

export default function CustomSyntaxHighlighter({ children }) {
  return (
    <SyntaxHighlighter
      language="javascript"
      style={style}
      customStyle={{ fontSize: 14, whiteSpace: 'pre-line' }}>
      {children}
    </SyntaxHighlighter>
  )
}
