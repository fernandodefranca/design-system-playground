import styled from 'styled-components'
import styles from '../styles'

const Text = styled.span`
  ${styles.type}
`

export default Text
