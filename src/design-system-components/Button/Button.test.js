import React from 'react'
import Component from './'
import renderer from 'react-test-renderer'

it('Renders correctly', () => {
  const tree = renderer.create(<Component />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "value" prop', () => {
  const tree = renderer.create(<Component value="Custom value" />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "mode" prop', () => {
  const tree = renderer.create(<Component mode="secondary" />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "size" prop', () => {
  const tree = renderer.create(<Component size="big" />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "disabled" prop', () => {
  const tree = renderer.create(<Component disabled={true} />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "loading" prop', () => {
  const tree = renderer.create(<Component loading={true} />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "fluid" prop', () => {
  const tree = renderer.create(<Component fluid={true} />).toJSON()
  expect(tree).toMatchSnapshot()
})
