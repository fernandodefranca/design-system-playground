import fn from './renderAsJSXString.js'

it('should output the expected JSX string', () => {
  const expected = `<Awesome foo="bar" answer={42} nope={null} />`
  const result = fn('Awesome', { foo: 'bar', answer: 42, nope: null })
  expect(result).toEqual(expected)
})
