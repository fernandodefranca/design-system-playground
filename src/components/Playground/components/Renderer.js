import React from 'react'
import PropTypes from 'prop-types'
import ErrorBoundary from '../../ErrorBoundary'

function stringify(args) {
  try {
    return JSON.stringify(args)
  } catch {
    return `${args}`
  }
}

export default function Renderer({ name, componentDefinition, props, info, loggerFn }) {
  const isValid = !!name || !!componentDefinition || !props
  if (!isValid) return null

  const Component = componentDefinition
  if (!Component) return null

  const propsClone = { ...props }

  // Provide stub handlers
  Object.keys(info.props).forEach((propName) => {
    const { type, description } = info.props[propName]
    const isHandlerFn = type.name === 'func' && propName.substr(0, 2) === 'on'

    if (isHandlerFn) {
      propsClone[propName] = (...args) => {
        loggerFn(`${propName}: ${description} ${stringify(args)}`)
      }
    }
  })

  return (
    <ErrorBoundary>
      <Component {...propsClone} />
    </ErrorBoundary>
  )
}

Renderer.propTypes = {
  name: PropTypes.string.isRequired,
  componentDefinition: PropTypes.object.isRequired,
  props: PropTypes.object.isRequired,
  loggerFn: PropTypes.func.isRequired,
}
