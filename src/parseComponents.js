const fs = require('fs')
const reactDocs = require('react-docgen')

const componentsPath = `${__dirname}/design-system-components`
const outputFilePath = `${__dirname}/parsed-components-info.json`
const componentsDirItems = fs.readdirSync(componentsPath)
const parsedComponents = {}

componentsDirItems.forEach((itemName) => {
  try {
    const filePath = `${componentsPath}/${itemName}/${itemName}.js`
    const data = fs.readFileSync(filePath)
    const componentInfo = reactDocs.parse(data)
    parsedComponents[componentInfo.displayName] = componentInfo
  } catch {
    // eslint-disable-next-line
  }
})

const outputContent = JSON.stringify(parsedComponents, null, 2)
fs.writeFile(outputFilePath, outputContent, () => {})
