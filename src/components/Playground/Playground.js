import React from 'react'

import SyntaxHighlighter from '../SyntaxHighlighter'
import Text from '../Text'

import Renderer from './components/Renderer'
import Selector from './components/Selector'
import PropsPanel from './components/PropsPanel'
import { Wrapper, LeftPanel, MainPanel, DemoPanel, CodePanel } from './components/LayoutElements'

import renderAsJSXString from '../../helpers/renderAsJSXString'
import getComponentDefaultProps from '../../helpers/getComponentDefaultProps'

const CallToAction = () => (
  <Text>
    <h2 style={{ margin: 0 }}>Design system component playground</h2>
    <p>Please, choose a component at the left panel.</p>
  </Text>
)

export default class Playground extends React.Component {
  state = {
    selectedComponentName: '',
    currentProps: {},
    consoleLogs: [],
  }

  pushMessageToConsoleLogs = (message) => {
    const { consoleLogs } = this.state
    this.setState({ consoleLogs: [message, ...consoleLogs] })
  }

  handleSelectionChange = (evt) => {
    const selectedComponentName = evt.target.value
    const consoleLogs = []

    // No component selected
    if (!selectedComponentName) {
      this.setState({ selectedComponentName, consoleLogs, currentProps: {} })
      return
    }

    // Selected a component - try to pass default props
    try {
      const { componentsInfo } = this.props
      const componentInfo = componentsInfo[selectedComponentName]
      const defaultProps = getComponentDefaultProps(componentInfo.props)
      this.setState({ selectedComponentName, consoleLogs, currentProps: defaultProps || {} })
    } catch (err) {
      console.error(err.message)
    }
  }

  handlePropUpdate = (evt) => {
    const { name, value } = evt.target
    const { currentProps } = this.state
    this.setState({
      currentProps: {
        ...currentProps,
        [name]: value,
      },
    })
  }

  render() {
    const { state, handleSelectionChange, handlePropUpdate, pushMessageToConsoleLogs } = this
    const { selectedComponentName, currentProps, consoleLogs } = state
    const { componentsInfo, componentsDefs } = this.props

    const selectorOptions = Object.keys(componentsDefs)
    const componentInfo = componentsInfo[selectedComponentName]
    const componentDefinition = componentsDefs[selectedComponentName]
    const jsxString = renderAsJSXString(selectedComponentName, currentProps)

    return (
      <Wrapper className="Playground">
        <LeftPanel>
          <Selector
            value={selectedComponentName}
            options={selectorOptions}
            onChange={handleSelectionChange}
          />
          <PropsPanel
            componentInfo={componentInfo}
            currentProps={currentProps}
            onChange={handlePropUpdate}
          />
        </LeftPanel>
        <MainPanel>
          <DemoPanel>
            {!selectedComponentName ? (
              <CallToAction />
            ) : (
              <Renderer
                name={selectedComponentName}
                info={componentsInfo[selectedComponentName]}
                componentDefinition={componentDefinition}
                props={currentProps}
                key={selectedComponentName}
                loggerFn={pushMessageToConsoleLogs}
              />
            )}
            <pre>{consoleLogs.join('\n')}</pre>
          </DemoPanel>
          <CodePanel>{jsxString && <SyntaxHighlighter>{jsxString}</SyntaxHighlighter>}</CodePanel>
        </MainPanel>
      </Wrapper>
    )
  }
}
