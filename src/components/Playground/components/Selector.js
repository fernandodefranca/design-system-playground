import React from 'react'
import PropTypes from 'prop-types'

export default function Selector({ value, options, onChange }) {
  return (
    <React.Fragment>
      <select name="selector" value={value} onChange={onChange}>
        {!value && <option value="">Choose a component</option>}
        {options.map((name) => {
          return (
            <option value={name} key={name}>
              {name}
            </option>
          )
        })}
      </select>
    </React.Fragment>
  )
}

Selector.propTypes = {
  value: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
}
