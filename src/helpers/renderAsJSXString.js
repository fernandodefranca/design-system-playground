export default function renderAsJSXString(componentName, propsValues) {
  if (!componentName) return ''
  const propsString = Object.keys(propsValues).reduce((acc, curr) => {
    const key = curr
    const value = propsValues[key]

    if (typeof value === 'string') return `${acc} ${key}="${value}"`
    return `${acc} ${key}={${value}}`
  }, '')
  return `<${componentName}${propsString} />`
}
