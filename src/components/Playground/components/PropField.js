import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Input = styled.input`
  width: 192px;
  font-size: 14px;
  padding: 8px;
`

function PropField({ fieldType, fieldName, fieldValue, onChange }) {
  if (fieldType.name === 'func') return null

  if (fieldType.name === 'enum') {
    if (!fieldType.value) return null
    const options = fieldType.value.map(({ value }) => value.split("'").join(''))
    return (
      <select name={fieldName} defaultValue={fieldValue} onChange={onChange}>
        {!fieldValue && <option value="">Select an option</option>}
        {options.map((option) => (
          <option value={option} key={option}>
            {option}
          </option>
        ))}
      </select>
    )
  }

  if (fieldType.name === 'bool') {
    const handlerFn = (evt) => {
      const { name, checked } = evt.target
      onChange({ target: { name, value: checked } })
    }
    return (
      <Input type="checkbox" name={fieldName} onChange={handlerFn} defaultChecked={fieldValue} />
    )
  }

  if (fieldType.name === 'number') {
    return <Input type="number" name={fieldName} onChange={onChange} value={fieldValue} />
  }

  return (
    <Input
      type="text"
      name={fieldName}
      onChange={onChange}
      value={fieldValue.split("'").join('')}
    />
  )
}

PropField.propTypes = {
  fieldType: PropTypes.object.isRequired,
  fieldName: PropTypes.string.isRequired,
  fieldValue: PropTypes.any,
  onChange: PropTypes.func.isRequired,
}

PropField.defaultProps = {
  fieldValue: '',
}

export default React.memo(PropField)
