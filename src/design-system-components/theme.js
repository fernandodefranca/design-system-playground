const colors = {
  primary: '#000',
  secondary: '#fff',
}

const sizes = {
  regular: '16px',
  big: '32px',
}

const padding = {
  regular: '12px',
  big: '24px',
}

const backgroundColor = {
  primary: '#ff8000',
  secondary: '#000000',
}

export default { colors, sizes, padding, backgroundColor }
